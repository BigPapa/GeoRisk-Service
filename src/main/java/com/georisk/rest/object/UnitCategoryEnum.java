package com.georisk.rest.object;

public enum UnitCategoryEnum {
  RESIDENTIAL("1"),
  INDUSTRIAL1("2"),
  INDUSTRIAL2("3"),
  INSTITUTION("4"),
  COMMERCIAL("5"),
  OTHER("9");

  String  code;

  UnitCategoryEnum(String code) {
    this.code = code;
  }

//  public UnitCategoryEnum getUnitCategoryEnum(String code) {
//
//  }

}
