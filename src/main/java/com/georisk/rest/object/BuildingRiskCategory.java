package com.georisk.rest.object;

import org.apache.commons.lang.StringUtils;
import org.opengis.feature.simple.SimpleFeature;

public class BuildingRiskCategory {
  //  private int minFloor;
  //  private int maxFloor;
  //  private int minHousing;
  //  private int maxHousing;
  //
  public static final String CODE_UTILISATION = "CODE_UTILISATION";
  public static final String LIBELLE_UTILISATION = "LIBELLE_UTILISATION";
  public static final String NOMBRE_LOGEMENT = "NOMBRE_LOGEMENT";
  public static final String ETAGE_HORS_SOL = "ETAGE_HORS_SOL";

  public static final String RESIDENTIAL = "Residential";
  public static final String COMMERCIAL = "Commercial";
  public static final String INDUSTRIAL = "Industrial";
  public static final String INSTITUTION = "Institution";

  //
  //
  //  public BuildingRiskCategory() {
  //
  //  }
  //
  public static RiskCategoryEnum returnRisk(SimpleFeature feature) {
    RiskCategoryEnum risk = null;
    if (feature != null) {
      String code = (String) feature.getAttribute("CODE_UTILISATION");
      Long housingNbObj = (Long) feature.getAttribute("NOMBRE_LOGEMENT");
      Long floorsObj = (Long) feature.getAttribute("ETAGE_HORS_SOL");
//      String code_utilisation = (String) feature.getAttribute("CODE_UTILISATION");

//      int housingNb = StringUtils.isNumeric(housingNbStr) ? Integer.parseInt(housingNbStr) : 0;
//      int floors = StringUtils.isNumeric(floorsStr) ? Integer.parseInt(floorsStr) : 0;

      UnitCategoryEnum unit = returnUtilisation(code);

      long housingNb = housingNbObj != null ? housingNbObj : 0;
      long floors = floorsObj != null ? floorsObj : 0;

      switch (unit) {
      case RESIDENTIAL:
        risk = evaluateResidentialRisk(housingNb, floors);
        break;
      case INDUSTRIAL1:
        risk = evaluateIndustrialRisk(housingNb, floors);
        break;
      case INDUSTRIAL2:
        risk = evaluateIndustrialRisk(housingNb, floors);
        break;
      case COMMERCIAL:
        risk = evaluateCommercialRisk(housingNb, floors);
        break;
      default :
        risk = RiskCategoryEnum.NONE;
        break;
      }
    }

    return risk;
  }

  private static UnitCategoryEnum returnUtilisation(String code_utilisation) {
    UnitCategoryEnum buildingType = UnitCategoryEnum.OTHER;
    if (!StringUtils.isBlank(code_utilisation)) {
      String firstChar = code_utilisation.substring(0, 1);
      switch (firstChar) {
      case "1":
        buildingType = UnitCategoryEnum.RESIDENTIAL;
        break;
      case "2":
        buildingType = UnitCategoryEnum.INDUSTRIAL1;
        break;
      case "3":
        buildingType = UnitCategoryEnum.INDUSTRIAL2;
        break;
      case "4":
        buildingType = UnitCategoryEnum.INSTITUTION;
        break;
      case "5":
        buildingType = UnitCategoryEnum.COMMERCIAL;
        break;
      default:
        buildingType = UnitCategoryEnum.OTHER;
        break;
      }
    }

      return buildingType;
    }

    private static RiskCategoryEnum evaluateResidentialRisk(long housingNb, long floors) {
    RiskCategoryEnum risk = null;
      if(housingNb <= 2 && floors <= 2) {
        risk = RiskCategoryEnum.WEAK;
      } else if(floors <= 3 && housingNb >=3 && housingNb <= 8) {
        risk = RiskCategoryEnum.MEDIUM;
      } else if((floors <= 6 && housingNb >= 9) || (floors >= 4 && floors <= 6 && housingNb <= 8)) {
        risk = RiskCategoryEnum.HIGH;
      } else if (floors >= 7) {
        risk = RiskCategoryEnum.SEVERE;
      } else {
        risk = RiskCategoryEnum.NONE;
      }

      return risk;
    }

  private static RiskCategoryEnum evaluateIndustrialRisk(long housingNb, long floors) {
    RiskCategoryEnum risk = null;
    if(floors <= 2) {
      risk = RiskCategoryEnum.MEDIUM;
    } else if(floors <= 6) {
      risk = RiskCategoryEnum.HIGH;
    } else if(floors >= 7) {
      risk = RiskCategoryEnum.SEVERE;
    } else {
      risk = RiskCategoryEnum.NONE;
    }

    return risk;
  }

  private static RiskCategoryEnum evaluateCommercialRisk(long housingNb, long floors) {
    RiskCategoryEnum risk = null;
    if(floors <= 2) {
      risk = RiskCategoryEnum.MEDIUM;
    } else if(floors <= 6) {
      risk = RiskCategoryEnum.HIGH;
    } else if(floors >= 7) {
      risk = RiskCategoryEnum.SEVERE;
    } else {
      risk = RiskCategoryEnum.NONE;
    }

    return risk;
  }

}
