package com.georisk.rest.object;

import java.util.Random;

public enum RiskCategoryEnum {
  NONE("NONE", "GREEN"), WEAK("WEAK", "LIGHTGREEN"), MEDIUM("MEDIUM", "YELLOW"), HIGH("HIGH", "ORANGE"), SEVERE("SEVERE", "RED");

  String name;
  String color;

  RiskCategoryEnum(String name, String color) {
    this.name = name;
    this.color = color;
  }

  public String getName() {
    return this.name;
  }

  public String getColor() {
    return this.color;
  }

  public RiskCategoryEnum getRiskCategoryEnum(String name) {
    return RiskCategoryEnum.valueOf("name");
  }
  public static RiskCategoryEnum getRandom() {
    Random random = new Random();
    return values()[random.nextInt(values().length)];
  }
}
