package com.georisk.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.georisk.geojson.GeoCharger;


@RestController
/**
 * Article Controller S
 */
public class DataController {

  /**
   * @param id
   * @return
   */
  @RequestMapping("/pull")
  public String detail(@RequestParam(value = "set") String set
		  , @RequestParam(value = "lat1") String lat1
		  , @RequestParam(value = "lon1") String lon1
		  , @RequestParam(value = "lat2") String lat2
		  , @RequestParam(value = "lon2") String lon2) {
    return GeoCharger.get(set, lat1, lon1, lat2, lon2);
  }
}