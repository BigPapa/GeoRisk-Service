package com.georisk.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.georisk.geojson.GeoCharger;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		new GeoCharger();
		SpringApplication.run(Application.class, args);
	}

}