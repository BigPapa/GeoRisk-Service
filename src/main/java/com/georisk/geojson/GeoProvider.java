package com.georisk.geojson;

import java.math.BigDecimal;

public abstract class GeoProvider {
	
	public GeoProvider() {
		init();
	}
	
	public abstract void init();
	public abstract String fetch(BigDecimal lat1, BigDecimal lon1, BigDecimal lat2, BigDecimal lon2);
}
