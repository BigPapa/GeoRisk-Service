package com.georisk.geojson;

import java.math.BigDecimal;
import java.util.HashMap;

import com.georisk.provider.FireRiskProvider;
import com.georisk.provider.GeoJsonProvider;
import com.georisk.provider.IncidentProvider;
import com.georisk.provider.WeatherProvider;

public class GeoCharger {

	private static HashMap<String, GeoProvider> providers;

	static {
		init();
	}

	public static void init() {
		providers = new HashMap<>();

		providers.put("INCIDENTS", new IncidentProvider());
		providers.put("WEATHER", new WeatherProvider());

		providers.put("CRUES", new GeoJsonProvider("CRUES",
				"./src/main/resources/data/vulnerabilites/vulnerabilitecrues2016.geojson", true));
		providers.put("PLUIES", new GeoJsonProvider("PLUIES",
				"src/main/resources/data/vulnerabilites/vulnerabilitepluiesabondantes2016.geojson", true));
		providers.put("SECHERESSES", new GeoJsonProvider("SECHERESSES",
				"src/main/resources/data/vulnerabilites/vulnerabilitesecheresses2016.geojson", true));
		providers.put("TEMPETES", new GeoJsonProvider("TEMPETES",
				"src/main/resources/data/vulnerabilites/vulnerabilitetempetesdestructrices2016.json", true));
		providers.put("CHALEUR", new GeoJsonProvider("CHALEUR",
				"src/main/resources/data/vulnerabilites/vulnerabilitevagueschaleur2016.geojson", true));
		providers.put("BOIS", new GeoJsonProvider("BOIS", "src/main/resources/data/bois.geojson", true));
		providers.put("POLICES", new GeoJsonProvider("POLICES", "src/main/resources/data/pdq_point.geojson", true));

		providers.put("INCENDIE", new FireRiskProvider());

		String pourt = "kjl";
	}

	public static String get(String set, String lat1, String lon1, String lat2, String lon2) {
		return providers.get(set).fetch(new BigDecimal(lat1), new BigDecimal(lon1), new BigDecimal(lat2),
				new BigDecimal(lon2));
	}

}
