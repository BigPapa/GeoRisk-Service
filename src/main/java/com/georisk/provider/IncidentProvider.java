package com.georisk.provider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.georisk.dataprovider.Coordinate;
import com.georisk.dataprovider.Incident;
import com.georisk.geojson.GeoProvider;

public class IncidentProvider extends GeoProvider {

	private static List<Incident> incidents = null;
	private static final String INCIDENTS_PATH = "src/main/resources/data/incidents/donneesouvertes-interventions-sim.csv";
	private static final int INCIDENT_DATE_TIME_INDEX = 1;
	private static final int INCIDENT_TYPE_DESC_INDEX = 2;
	private static final int INCIDENT_DESCRIPTION_GROUPE = 3;
	private static final int INCIDENT_CASERNE_INDEX = 4;
	private static final int INCIDENT_NOM_VILLE_INDEX = 5;
	private static final int INCIDENT_NOM_ARRONDISSENENT_INDEX = 6;
	private static final int INCIDENT_DIVISION_INDEX = 7;
	private static final int INCIDENT_LATITUDE_INDEX = 8;
	private static final int INCIDENT_LONGITUDE_INDEX = 9;
	private static final int INCIDENT_NOMBRE_UNITE_INDEX = 10;

	@Override
	public void init() {
		String csvFile = INCIDENTS_PATH;

		String line = "";
		String cvsSplitBy = ",";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		incidents = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile));) {

			/* skip first line */
			line = br.readLine();

			while ((line = br.readLine()) != null) {

				String[] data = line.split(cvsSplitBy);

				LocalDateTime localDateTime = LocalDateTime.parse(data[INCIDENT_DATE_TIME_INDEX], formatter);
				String typeDescription = data[INCIDENT_TYPE_DESC_INDEX];
				String descriptionGroupe = data[INCIDENT_DESCRIPTION_GROUPE];
				String caserne = data[INCIDENT_CASERNE_INDEX];
				String nomVille = data[INCIDENT_NOM_VILLE_INDEX];
				String nomArondissement = data[INCIDENT_NOM_ARRONDISSENENT_INDEX];
				String division = data[INCIDENT_DIVISION_INDEX];
				BigDecimal latitude = new BigDecimal(data[INCIDENT_LATITUDE_INDEX]);
				BigDecimal longitude = new BigDecimal(data[INCIDENT_LONGITUDE_INDEX]);

				int nombreUnite = 0;

				/* The number of units might not be declared */
				if (data.length == 11) {
					nombreUnite = Integer.parseInt(data[INCIDENT_NOMBRE_UNITE_INDEX]);
				}

				Coordinate coordinate = new Coordinate(latitude, longitude);
				Incident incident = new Incident(coordinate, localDateTime, typeDescription, descriptionGroupe, caserne,
						nomVille, nomArondissement, division, nombreUnite);

				incidents.add(incident);

			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String fetch(BigDecimal lat1, BigDecimal lon1, BigDecimal lat2, BigDecimal lon2) {
		JSONObject featureCollection = new JSONObject();
		try {
			featureCollection.put("type", "featureCollection");

			JSONArray featureList = new JSONArray();

			for (Incident incident : incidents) {
				if (incident.getCoordinate().getLatitude().compareTo(lat1) >= 0
						&& incident.getCoordinate().getLatitude().compareTo(lat2) <= 0
						&& incident.getCoordinate().getLongitude().compareTo(lon1) >= 0
						&& incident.getCoordinate().getLongitude().compareTo(lon2) <= 0) {
					JSONObject point = new JSONObject();
					point.put("type", "Point");
					// construct a JSONArray from a string; can also use an array or list
					JSONArray coord = new JSONArray("[" + incident.getCoordinate().getLatitude() + ","
							+ incident.getCoordinate().getLongitude() + "]");
					point.put("coordinates", coord);
					JSONObject feature = new JSONObject();
					feature.put("geometry", point);
					
					JSONObject props = new JSONObject();

					props.put("datetime", incident.getLocalDateTime());
					props.put("type_description", incident.getTypeDescription());
					props.put("description_groupe", incident.getDescriptionGroupe());
					props.put("caserne", incident.getCaserne());
					props.put("nom_ville", incident.getNomVille());
					props.put("nom_arrondissement", incident.getNomArrondissement());
					props.put("division", incident.getDivision());

					feature.put("properties", props);
					featureList.put(feature);
					featureCollection.put("features", featureList);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return featureCollection.toString();
	}

}
