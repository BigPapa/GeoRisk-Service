package com.georisk.provider;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;

import org.geotools.factory.CommonFactoryFinder;
import org.geotools.factory.GeoTools;
import org.geotools.feature.FeatureCollection;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.PropertyName;
import org.opengis.filter.spatial.Intersects;

import com.georisk.geojson.GeoProvider;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

public class GeoJsonProvider extends GeoProvider {

	@SuppressWarnings("rawtypes")
	private FeatureCollection collection;
	private boolean inverseLat;

	public GeoJsonProvider(String name, String dataSourceFile, boolean inverseLat) {
		super();
		
		this.inverseLat = inverseLat;

		FeatureJSON g = new FeatureJSON(new GeometryJSON(15));

		try {
			collection = g.readFeatureCollection(new FileInputStream(dataSourceFile));
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("---- " + name + " : " + collection.size());

	}

	@Override
	public void init() {}

	@Override
	public String fetch(BigDecimal lat1, BigDecimal lon1, BigDecimal lat2, BigDecimal lon2) {
		FeatureJSON g = new FeatureJSON(new GeometryJSON(15));
		GeometryFactory fac = new GeometryFactory();

		Geometry referenceGeom;
		double minx = lat1.doubleValue();
		double miny = lon1.doubleValue();
		double maxx = lat2.doubleValue();
		double maxy = lon2.doubleValue();

		Envelope env = new Envelope(minx, maxx, miny, maxy);
		if (inverseLat) {
			//pour les boulets qui ont mis la longitude en premier dans leurs données
			env = new Envelope(miny, maxy, minx, maxx);
		}
		
		referenceGeom = fac.toGeometry(env);
		String geometryAttributeName = "geometry";

		FilterFactory2 filterFactory2 = CommonFactoryFinder.getFilterFactory2(GeoTools.getDefaultHints());

		final PropertyName geomAttribute = filterFactory2.property(geometryAttributeName);
		Intersects filter = filterFactory2.intersects(geomAttribute, filterFactory2.literal(referenceGeom));

		@SuppressWarnings("rawtypes")
		FeatureCollection res = collection.subCollection(filter);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			g.writeFeatureCollection(res, baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toString();
	}

}
