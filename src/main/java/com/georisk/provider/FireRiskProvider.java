package com.georisk.provider;

import com.georisk.geojson.GeoProvider;
import com.georisk.rest.object.BuildingRiskCategory;
import com.georisk.rest.object.RiskCategoryEnum;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.factory.GeoTools;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.PropertyName;
import org.opengis.filter.spatial.Intersects;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class FireRiskProvider extends GeoProvider {

  private static final String FIRERISK_PATH = "src/main/resources/data/firerisk/uniteevaluationfonciere.geojson";
  private static final String REGION_PATH = "src/main/resources/data/firerisk/region.geojson";

  private FeatureCollection fcRegion;
  private FeatureCollection fcFireRisk;

  private FeatureCollection generateFeaturesWithRisk() throws IOException {
    FileInputStream input = new FileInputStream(FIRERISK_PATH);
    FeatureJSON g = new FeatureJSON(new GeometryJSON(15));

    FeatureCollection fc1 = g.readFeatureCollection(input);

    SimpleFeatureType type = (SimpleFeatureType) fc1.getSchema();

    SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
    builder.addAll(type.getAttributeDescriptors());
    builder.setName("FeatureCollection");
    builder.add("RISK", String.class);
    builder.add("RISKCOLOR", String.class);
    SimpleFeatureType newType = builder.buildFeatureType();
    SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(newType);

    List<SimpleFeature> features = new ArrayList<>();
    FeatureIterator<SimpleFeature> iterator = fc1.features();

    try {
      while (iterator.hasNext()) {
        SimpleFeature feature = iterator.next();

          RiskCategoryEnum risk = BuildingRiskCategory.returnRisk(feature);
            if(risk != RiskCategoryEnum.NONE) {
              featureBuilder.reset();
              featureBuilder.addAll(feature.getAttributes());
              featureBuilder.set("RISK", risk.getName());
              featureBuilder.set("RISKCOLOR", risk.getColor());
              SimpleFeature newFeature = featureBuilder.buildFeature(null);
              features.add(newFeature);
            }
      }

    } finally {
      iterator.close();
    }

    return  new ListFeatureCollection(newType, features);
  }

  private FeatureCollection generateRegionFeaturesWithRisk() throws IOException {
    FeatureJSON featureJSON = new FeatureJSON(new GeometryJSON(15));
    FeatureCollection fcRegion = featureJSON.readFeatureCollection(new FileInputStream(REGION_PATH));
    SimpleFeatureType type = (SimpleFeatureType) fcRegion.getSchema();

    SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
    builder.addAll(type.getAttributeDescriptors());
    builder.setName("FeatureCollection");
    builder.add("RISK", String.class);
    builder.add("RISKCOLOR", String.class);
    SimpleFeatureType newType = builder.buildFeatureType();
    SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(newType);
    List<SimpleFeature> newRegionFeatures = new ArrayList<>();

    FeatureIterator<SimpleFeature> iteratorRegion = fcRegion.features();
    try {
      while (iteratorRegion.hasNext()) {
        SimpleFeature featureRegion = iteratorRegion.next();
        MultiPolygon defaultGeometryRegion = (MultiPolygon) featureRegion.getDefaultGeometry();

        FeatureIterator<SimpleFeature> featureIterator = this.fcFireRisk.features();
        Map<RiskCategoryEnum, Integer> riskRegion = new HashMap<>();
        try {
          while (featureIterator.hasNext()) {
            SimpleFeature feature = featureIterator.next();
            Polygon defaultGeometry = (Polygon) feature.getDefaultGeometry();

            if (defaultGeometryRegion.contains(defaultGeometry.getCentroid())) {
              incrementRisk(riskRegion, feature);
            }
          }

          RiskCategoryEnum averageRisk = computeAverageRisk(riskRegion);
          featureBuilder.reset();
          featureBuilder.addAll(featureRegion.getAttributes());
          featureBuilder.set("RISK", averageRisk.getName());
          featureBuilder.set("RISKCOLOR", averageRisk.getColor());
          SimpleFeature newFeature = featureBuilder.buildFeature(null);
          newRegionFeatures.add(newFeature);
        } finally {
          featureIterator.close();
        }
      }
    } finally {
      iteratorRegion.close();
    }

    return new ListFeatureCollection(newType, newRegionFeatures);
  }

  private Map<RiskCategoryEnum, Integer> incrementRisk(Map<RiskCategoryEnum, Integer> riskMap, SimpleFeature feature) {
    String risk = (String) feature.getAttribute("RISK");

    Integer count = riskMap.get(RiskCategoryEnum.valueOf(risk));
    count = count == null ? 1 : count + 1;
    riskMap.put(RiskCategoryEnum.valueOf(risk), count);

    return riskMap;
  }

  private RiskCategoryEnum computeAverageRisk(Map<RiskCategoryEnum, Integer> riskMap) {
    int max = 0;
    RiskCategoryEnum riskMax = null;

    Iterator<RiskCategoryEnum> it = riskMap.keySet().iterator();
    while (it.hasNext()) {
      RiskCategoryEnum risk = it.next();
      Integer count = riskMap.get(risk);
      if(count > max) {
        riskMax = risk;
      }
    }

    // To illustrate different scenarios
    return RiskCategoryEnum.getRandom();

    //return riskMax != null ? riskMax : RiskCategoryEnum.NONE;
  }

  @Override
  public void init() {
    try {
      this.fcFireRisk = generateFeaturesWithRisk();
      this.fcRegion = generateRegionFeaturesWithRisk();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String fetch(BigDecimal lat1, BigDecimal lon1, BigDecimal lat2, BigDecimal lon2) {
    FeatureJSON g = new FeatureJSON(new GeometryJSON(15));
    GeometryFactory fac = new GeometryFactory();

    // Filter not operational
    /*Geometry referenceGeom;
    double minx = lat1.doubleValue();
    double miny = lon1.doubleValue();
    double maxx = lat2.doubleValue();
    double maxy = lon2.doubleValue();

    Envelope env = new Envelope(minx, maxx, miny, maxy);
    referenceGeom = fac.toGeometry(env);
    String geometryAttributeName = "geometry";

    FilterFactory2 filterFactory2 = CommonFactoryFinder.getFilterFactory2(GeoTools.getDefaultHints());

    final PropertyName geomAttribute = filterFactory2.property(geometryAttributeName);
    Intersects filter = filterFactory2.intersects(geomAttribute, filterFactory2.literal(referenceGeom));*/

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      g.writeFeatureCollection(this.fcRegion, baos);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return baos.toString();
  }
}
