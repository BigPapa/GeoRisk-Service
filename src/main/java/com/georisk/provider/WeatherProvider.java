package com.georisk.provider;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.georisk.dataprovider.Weather;
import com.georisk.geojson.GeoProvider;

public class WeatherProvider extends GeoProvider {

	private static Map<LocalDateTime, Weather> weathers = null;
	private static final String STATION_DATA = "stationdata";
	private static final String DAY = "day";
	private static final String HOUR = "hour";
	private static final String MINUTE = "minute";
	private static final String MONTH = "month";
	private static final String YEAR = "year";
	private static final String TEMP = "temp";
	private static final String WIND_DIR = "winddir";
	private static final String WIND_SPEED = "windspd";
	private static final String UNITS = "units";
	private static final String CELSIUS = "°C";
	private static final String KMH = "km/h";
	private static final String TENTH_DEGREE = "10s deg";
	private static final String WEATHER_PATH = "src/main/resources/data/weather";
	
	private static Float retrieveChildNodeContent(String childName, String attributeName, String attributeValue, Element element) {
		
		NodeList children = element.getElementsByTagName(childName);
		
		/* For each child, get data if attribute name and attribute value match parameter */
		for(int j = 0 ; j < children.getLength() ; j++) {
			Node child = children.item(j);
			if(child.getNodeType() == Node.ELEMENT_NODE) {
				Element childElement = (Element) child;
				if(childElement.getAttribute(attributeName).equals(attributeValue) && !childElement.getTextContent().trim().isEmpty()) {
					return Float.parseFloat(childElement.getTextContent());
				}
			}
		}
		
		return null;
		
	}

	public static Map<LocalDateTime, Weather> getWeathers() {
		return weathers;
	}


	public static void setWeathers(Map<LocalDateTime, Weather> weathers) {
		WeatherProvider.weathers = weathers;
	}


	@Override
	public void init() {
		weathers = new HashMap<>();
		
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			File folder = new File(WEATHER_PATH);
			File[] listOfFiles = folder.listFiles();
	
			/* For each file containing weather content*/
			for (File file : listOfFiles) {
			    if(file.isFile()) {
					Document doc = dBuilder.parse(file);
					doc.getDocumentElement().normalize();
					NodeList nodeList = doc.getElementsByTagName(STATION_DATA);
					
					/* For each station data node*/
					for (int i = 0 ; i < nodeList.getLength() ; i++) {
						Node node = nodeList.item(i);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							Element element = (Element) node;
							
							/* Fetch date time info */
							int day = Integer.parseInt(element.getAttribute(DAY));
							int hour = Integer.parseInt(element.getAttribute(HOUR));
							int minute = Integer.parseInt(element.getAttribute(MINUTE));
							int month = Integer.parseInt(element.getAttribute(MONTH));
							int year = Integer.parseInt(element.getAttribute(YEAR));
							
							/* Fetch weather info from children */
							Float celsius = retrieveChildNodeContent(TEMP, UNITS, CELSIUS, element);
							Float windSpeed = retrieveChildNodeContent(WIND_SPEED, UNITS, KMH, element);
							Float windDir = retrieveChildNodeContent(WIND_DIR, UNITS, TENTH_DEGREE, element);
							
							LocalDateTime localDateTime = LocalDateTime.of(year, month, day, hour, minute);
							Weather weather = new Weather(celsius, windSpeed, windDir);
							
							weathers.put(localDateTime, weather);
						}
					}
			    }
			}

		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}


	@Override
	public String fetch(BigDecimal lat1, BigDecimal lon1, BigDecimal lat2, BigDecimal lon2) {
		return null;
	}
	
}
