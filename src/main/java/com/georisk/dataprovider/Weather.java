package com.georisk.dataprovider;


public class Weather {

	private Float celsius;
	private Float windSpeed;
	private Float windDir;

	public Weather(Float celsius, Float windSpeed, Float windDir) {
		this.celsius = celsius;
		this.windSpeed = windSpeed;
		this.windDir = windDir;
	}

	public float getCelsius() {
		return celsius;
	}

	public void setCelsius(float celsius) {
		this.celsius = celsius;
	}

	public float getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(float windSpeed) {
		this.windSpeed = windSpeed;
	}

	public float getWindDir() {
		return windDir;
	}

	public void setWindDir(float windDir) {
		this.windDir = windDir;
	}

}
