package com.georisk.dataprovider;

import java.math.BigDecimal;


public class Coordinate {

	private BigDecimal latitude;
	private BigDecimal longitude;
	
	
	public Coordinate(BigDecimal latitude, BigDecimal longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getLatitude().hashCode();
		result = prime * result + getLongitude().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof Coordinate) {
	
			Coordinate instance = (Coordinate) obj;
			
			/* Check if latitude and longitude coordinates are the same */
			int latitudeComparison = this.getLatitude().compareTo(instance.getLatitude());
			int longitudeComparison = this.getLongitude().compareTo(instance.getLongitude());
			
			if (latitudeComparison == 0 && longitudeComparison == 0) {
				return true;
			}
		} 
		
		return false;
		
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
}
