package com.georisk.dataprovider;

import java.time.LocalDateTime;

public class Incident {

	private Coordinate coordinate;
	private LocalDateTime localDateTime;
	private String typeDescription;
	private String descriptionGroupe;
	private String caserne;
	private String nomVille;
	private String nomArrondissement;
	private String division;
	private int nombreUnite;

	public Incident(Coordinate coordinate, LocalDateTime localDateTime, String typeDescription,
			String descriptionGroupe, String caserne, String nomVille, String nomArrondissement, String division, int nombreUnite) {
		this.setCoordinate(coordinate);
		this.setLocalDateTime(localDateTime);
		this.setTypeDescription(typeDescription);
		this.setDescriptionGroupe(descriptionGroupe);
		this.setCaserne(caserne);
		this.setNomVille(nomVille);
		this.setNomArrondissement(nomArrondissement);
		this.setDivision(division);
		this.setNombreUnite(nombreUnite);
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}

	public String getTypeDescription() {
		return typeDescription;
	}

	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}

	public String getDescriptionGroupe() {
		return descriptionGroupe;
	}

	public void setDescriptionGroupe(String descriptionGroupe) {
		this.descriptionGroupe = descriptionGroupe;
	}

	public String getCaserne() {
		return caserne;
	}

	public void setCaserne(String caserne) {
		this.caserne = caserne;
	}

	public String getNomVille() {
		return nomVille;
	}

	public void setNomVille(String nomVille) {
		this.nomVille = nomVille;
	}

	public String getNomArrondissement() {
		return nomArrondissement;
	}

	public void setNomArrondissement(String nomArrondissement) {
		this.nomArrondissement = nomArrondissement;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public int getNombreUnite() {
		return nombreUnite;
	}

	public void setNombreUnite(int nombreUnite) {
		this.nombreUnite = nombreUnite;
	}

}
