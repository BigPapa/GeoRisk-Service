package com.georisk.rest;

import com.georisk.rest.controller.DataController;
import com.georisk.rest.service.ArticleService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MyTest {

        @MockBean
        private ArticleService remoteService;

        @Autowired
        private ApplicationContext applicationContext;

        @Autowired
        private TestRestTemplate restTemplate;

        @Before
        public void start() {
                BDDMockito.given(this.remoteService.getString()).willReturn("Yeeeeeeeeeeeeeessssssssssss");
        }

        @Test
        public void test() {
                DataController bean = applicationContext.getBean(DataController.class);
                System.out.println(bean);
        }

        @Test
        public void exampleTest() {
                String body = this.restTemplate.getForObject("/category/test1", String.class);
                System.out.println(body);
        }
}